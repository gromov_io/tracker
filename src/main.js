import Vue from 'vue'
import App from './App.vue'
/* eslint-disable */

Vue.util.defineReactive(Vue.prototype, '$tracking', tracking)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')